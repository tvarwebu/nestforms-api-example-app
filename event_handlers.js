window.onload = function () {
  const elementsToggle = document.getElementsByClassName('handler-element-toggle-visibility');
  for (const element of elementsToggle) {
    element.onclick = function () {
      const elementToToggle = element.dataset.elementIdentifier === 'nextSibling' ? element.nextSibling :  document.getElementById(element.dataset.elementIdentifier);
      elementToToggle.style.display = elementToToggle.style.display === 'none' ? 'block' : 'none';
    }
  }

  const elementsReplace = document.getElementsByClassName('handler-element-replace-text-content');
  for (const element of elementsReplace) {
    element.onclick = function () {
      element.textContent = element.dataset.textContentNew
    }
  }
}

window.addEventListener("message", (message) => {
  if (message.origin === window.host) {
    alert("Received message: " + message.data.event)
  }
}, false);
