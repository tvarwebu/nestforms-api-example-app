<?php
function exampleFormsResponsesCustomdb($accessToken, $provider, $client) {
  global $host;
  // some info output
  printNavigation();
  $post_data = null;

  if (isset($_REQUEST['customdb'])) {
    //Custom DB part
    try {
      //Some data updates
      if (!empty($_POST['act']) && $_POST['act'] === 'store_group_data') {
        $data = $_POST['group'];
        $data['items'] = $_POST['items'];
        $request = $provider->getAuthenticatedRequest(
          $_GET['group'] !== 'new' ? 'PUT' : 'POST',
          $host.'/api/customdb'.(!empty($_GET['customdb']) ? '/'.intval($_GET['customdb']).'/groups'.(!empty($_GET['group']) && $_GET['group'] !== 'new' ? '/'.intval($_GET['group']) : '') : ''),
          $accessToken
        );
        $post_data = $request->getMethod().' '.$request->getUri()."\r\n";
        foreach ($request->getHeaders() as $header => $values) {
          foreach ($values as $value) {
            $post_data .= $header .': '.(strToLower($header) === 'authorization' ? '** hidden **' : $value)."\r\n";
          }
        }
        $post_data .= "\r\n\r\n";

        if (!empty($data)) {
          $post_data .= http_build_query($data)."\r\n\r\n";
        }
        $response = $client->send($request, array('form_params' => $data));
        $post_data .= "Response: \n";
        $post_data .= $response->getStatusCode()." ".$response->getReasonPhrase()."\r\n";
        foreach ($response->getHeaders() as $header => $values) {
          foreach ($values as $value) {
            $post_data .= $header .': '.(strToLower($header) === 'authorization' ? '** hidden **' : $value)."\r\n";
          }
        }
        $post_data .= "\r\n\r\n";
        $updateStream = $response->getBody()->getContents();
        if (!empty($updateStream)) {
          $post_data.= $updateStream;
        }
        $data = json_decode($updateStream, true);
        $_GET['group'] = $data['customdb_group_id'];
      }
      if (!empty($_GET['delgroup'])) {
        $response = $client->send($provider->getAuthenticatedRequest(
          'DELETE',
          $host.'/api/customdb/'.intval($_GET['customdb']).'/groups/'.intval($_GET['delgroup']),
          $accessToken
        ));
        $updateStream = $response->getBody();
        $_GET['group'] = $_GET['delgroup'];
      }
    } catch (GuzzleHttp\Exception\ClientException $ex) {
      echo '<div style="color: red;">Failed to store the data: '.$ex.'</div>';
    }
    try {
      // reading the data
      $data = array();
      $responseStream = null;
      if (isset($_GET['group']) && $_GET['group'] === 'new') {
      } else {
        $start = 0;
        if (!empty($_REQUEST['pg'])) {
          $start = $_REQUEST['pg'] * 50;
        }
        $response = $client->send($provider->getAuthenticatedRequest(
          'GET',
          $host.'/api/customdb'.(!empty($_GET['customdb']) ? '/'.intval($_GET['customdb']).'/groups'.(!empty($_GET['group']) ? '/'.intval($_GET['group']) : '') : '').'?listing_start='.$start,
          $accessToken
        ));
        $responseStream = $response->getBody();
        $data = json_decode($responseStream, true);
      }
      if (empty($_GET['customdb'])) {
        printList('tables', $data);
      } elseif (!empty($_GET['group'])) {
        $response_columns = $client->send($provider->getAuthenticatedRequest(
          'GET',
          $host.'/api/customdb'.(!empty($_GET['customdb']) ? '/'.intval($_GET['customdb']) : ''),
          $accessToken
        ));
        $columnsResponseStream = $response_columns->getBody();
        $data_columns = json_decode($columnsResponseStream, true);
        printGroupDetails($data, $data_columns, array_column($data_columns['columns'], 'name', 'customdb_column_id'));
      } else {
        printList('groups', $data, '<a href="?customdb=0"> &laquo; back to list of Tables</a>');
      }
      if (!empty($post_data)) {
        printPostData($post_data);
      }
      printParsedData($data);
      printJSONString($responseStream);
    } catch (GuzzleHttp\Exception\ClientException $ex) {
      // if an error occurs
      echo '<div style="color: red;">Failed to fetch the data, you don\'t seem to have the required permissions</div>';
    }
  } elseif (isset($_REQUEST['members'])) {
    //Listing the members
    try {
      // get the data
      $response = $client->send($provider->getAuthenticatedRequest(
        'GET',
        $host.'/api/members',
        $accessToken
      ));
      $responseStream = $response->getBody();
      $data = json_decode($responseStream, true);
      printList('members', $data);
      if (!empty($post_data)) {
        printPostData($post_data);
      }
      printParsedData($data);
      printJSONString($responseStream);
    } catch (GuzzleHttp\Exception\ClientException $ex) {
      // if an error occurs
      echo '<div style="color: red;">Failed to fetch the data, you don\'t seem to have the required permissions</div>';
    }
  } elseif (empty($_REQUEST['fid']) || (int)$_REQUEST['fid'] <= 0) {
    //Listing forms
    try {
      // get the date
      $response = $client->send($provider->getAuthenticatedRequest(
        'GET',
        $host.'/api/forms_list',
        $accessToken
      ));
      $responseStream = $response->getBody();
      $forms = json_decode($responseStream, true);

      printList('forms', $forms);

      if (!empty($post_data)) {
        printPostData($post_data);
      }
      printParsedData($forms);
      printJSONString($responseStream);
    } catch (GuzzleHttp\Exception\ClientException $ex) {
      // if an error occurs
      echo '<div style="color: red;">Failed to fetch the data, you don\'t seem to have the required permissions</div>';
    }
  } else {
    //Listing responses
    try {
      // get the data
      $response = $client->send($provider->getAuthenticatedRequest(
        'GET',
        $host.'/api/report/'.((int)$_REQUEST['fid']).(!empty($_REQUEST['pg']) ? '?page='.((int)$_REQUEST['pg']) : ''),
        $accessToken
      ));
      $responseStream = $response->getBody();
      $details = json_decode($responseStream, true);

      // show some form info
      printFormDetail($details);

      // show form responses
      printResponses($details);
      if (!empty($post_data)) {
        printPostData($post_data);
      }
      printParsedData($details);
      printJSONString($responseStream);
    } catch (GuzzleHttp\Exception\ClientException $ex) {
      // if an error occurs
      echo '<div style="color: red;">Failed to fetch the data, you don\'t seem to have the required permissions</div>';
    }
  }
}

/**
 * If needed prints links for previous and next page.
 * @param array $items
 */
function prevNextPageFunction($items) {
  if (!empty($_REQUEST['pg']) && $_REQUEST['pg'] > 0) {
    parse_str($_SERVER['QUERY_STRING'], $query);
    $query['pg'] = (isset($query['pg']) ? (int)$query['pg'] : 0) - 1;
    echo '<a class="prev-page" href="?'.http_build_query($query).'">&lt; Previous Page</a>';
  }
  if (in_array(count($items), [50, 100, 1000])) {
    parse_str($_SERVER['QUERY_STRING'], $query);
    $query['pg'] = (isset($query['pg']) ? (int)$query['pg'] : 0) + 1;
    echo '<a class="next-page" href="?'.http_build_query($query).'">Next Page &gt;</a>';
  }
}

/**
 * Prints menu for navigating forms, members adn customdb tables examples.
 */
function printNavigation() {
  echo '<div>
    Submenu: 
    <a href="?page=example_forms_responses_customdb">Forms</a>
    <a href="?page=example_forms_responses_customdb&members=0">Members</a>
    <a href="?page=example_forms_responses_customdb&customdb=0">Customdb Tables</a>
  </div>';
}

/**
 * Prints list of items.
 * @param string $type Type of list, must be one of ['tables', 'groups', 'members', 'forms']
 * @param array $data Data from response to print.
 * @param string $returnButton HTML for return button in header.
 */
function printList($type, $data, $returnButton = '') {
  if (!in_array($type, ['tables', 'groups', 'members', 'forms'])) {
    echo '<span class="error">Unknown type: '.htmlspecialchars($type).'</span>';
    return;
  }
  echo '<h1>List of '.ucfirst($type).$returnButton.'</h1>';
  if (!empty($data)) {
    echo prevNextPageFunction($data);
    echo '<ul>';
    foreach ($data as $value) {
      switch ($type) {
        case 'tables':
          echo '  <li><a href="?customdb='.intval($value['customdb_table_id']).'">'.htmlspecialchars($value['name']).'</a></li>'."\n";
          break;
        case 'groups':
          echo '  <li><a href="?customdb='.intval($value['customdb_table_id']).'&group='.intval($value['customdb_group_id']).'">'.implode(', ', array_map('htmlspecialchars', array_column($value['items'], 'value'))).'</a></li>'."\n";
          break;
        case 'members':
          echo '  <li>'.htmlspecialchars($value['member_label']).'</li>'."\n";
          break;
        case 'forms':
          echo '  <li><a href="?fid='.intval($value['form_id']).'">'.htmlspecialchars($value['name']).'</a></li>'."\n";
          break;
      }
    }
    echo '</ul>'."\n";
    echo prevNextPageFunction($data);
    if ($type === 'groups') {
      echo ' <a href="?customdb='.intval($_GET['customdb']).'&group=new">+ Add new</a>';
    }
  } else {
    echo '<p>There are no Easter Eggs in this script</p>'."\n";
  }
}

/**
 * Prints Custom DB Group details.
 * @param array $data Response data to print.
 * @param array $data_columns Response column data to print.
 * @param array $columns array of column names with the key beaing column id.
 */
function printGroupDetails($data, $data_columns, $columns) {
  echo '<h1>Groups Details';
  echo ' <a href="?customdb='.intval($_GET['customdb']).'"> &laquo; back to list of Groups</a></h1>';

  echo '<form action="" method="POST"><pre>';
  echo '</pre>';
  echo '<h2>Group Details'.(!empty($data['customdb_group_id']) ? ' - <a href="?customdb='.intval($_GET['customdb']).'&delgroup='.intval($data['customdb_group_id']).'">Delete</a>':'').'</h2>';
  echo '<ul>';
  foreach (array(
    'group_status' => array(
      'type' => 'dropdown',
      'options' => array('visit' => 'visit', 'visited' => 'visited', 'approved' => 'approved', 'revisit' => 'revisit')
    ),
    'customdb_handle_id' => array(
      'type' => 'dropdown',
      'options' => array_column($data_columns['handles'], 'name', 'customdb_handle_id'),
    ),
    'is_active' => array(
      'type' => 'dropdown',
      'options' => array('0' => 'No', '1' => 'Yes')
    ),
    'map_lat' => array(
      'type' => 'number',
      'input_attributes' => array(
        'step' => '0.0001',
      )
    ),
    'map_lng' => array(
      'type' => 'number',
      'input_attributes' => array(
        'step' => '0.0001',
      )
    ),
    'planned_datetime' => array(
      'type' => 'datetime-local',
      'date_format' => 'Y-m-d\TH:i:s',
    ),
/*
    'response_frequency' => array(
      'type' => 'dropdown',
      'options' => array('')
    ),
*/
    'last_visit' => array(
      'type' => 'datetime-local',
      'date_format' => 'Y-m-d\TH:i:s',
    ),
    'has_members_mapping' => array(
      'type' => 'text'
    ) ) as $key=>$cnf) {
    echo '<li><label>'.(isset($cnf['title']) ? $cnf['title'] : $key).' ';
    if ($cnf['type'] === 'dropdown') {
      echo '<select name="group['.$key.']">';
      foreach ($cnf['options'] as $_key => $val) {
        $_key = trim($_key);
        echo '<option value="'.htmlspecialchars($_key, ENT_QUOTES).'"'.(isset($data[$key]) && $data[$key] == $_key ? ' selected="selected"' : '').'>'.htmlspecialchars($val).'</option>';
      }
      echo '</select>';
    } else {
      $attrs = array();
      if (!empty($cnf['input_attributes'])) {
        foreach ($cnf['input_attributes'] as $k => $val) {
          $attrs[] = $k.'="'.htmlspecialchars($val, ENT_QUOTES).'"';
        }
      }
      if (!empty($data[$key]) && isset($cnf['date_format'])) {
        $data[$key] = gmdate($cnf['date_format'], strToTime($data[$key].' GMT'));
      }
      echo '<input type="'.$cnf['type'].'" name="group['.$key.']" value="'.(isset($data[$key]) ? htmlspecialchars($data[$key], ENT_QUOTES) : '').'"'.(!empty($attrs) ? ' '.implode(' ', $attrs) : '').'>';
    }
    echo '</label></li>';
  }
  echo '</ul>';
  echo '<h2>Cell Values</h2>';
  echo '<ul>';
  foreach ($columns as $column_id=>$name) {
    $item = null;
    if (!empty($data['items'])) {
      foreach ($data['items'] as $item) {
        if ($item['customdb_column_id'] == $column_id) {
          break;
        }
        unset($item);
      }
    }
    if (empty($item['customdb_handle_id']) || $item['customdb_handle_id'] == $data['customdb_handle_id']) {
      echo '<li><label>'.htmlspecialchars($name).' <input type="text" name="items['.intval($column_id).']" value="'.(!empty($item) ? htmlspecialchars($item['value'], ENT_QUOTES) : '').'"></label></li>';
    }
  }
  echo '</ul>';
  echo '<div><input type="submit" value="Update" /><input type="hidden" name="act" value="store_group_data" /></div>';
  echo '</form>';
}

/**
 * Prints post data.
 * @param string $post_data Data to print.
 */
function printPostData($post_data) {
  echo '<h3 style="cursor: pointer" class="handler-element-toggle-visibility" data-element-identifier="hiddenpostdata">POSTed data (not an exact log of the HTTP communication, click to toggle):</h3>';
  echo '<pre id="hiddenpostdata" style="display: none">';
  echo $post_data;
  echo '</pre>';
}

/**
 * Prints parsed data.
 * @param array $data Data to print.
 */
function printParsedData($data) {
  echo '<h3 style="cursor: pointer" class="handler-element-toggle-visibility" data-element-identifier="hiddenpre">Parsed data (click to toggle):</h3>'."\n";
  echo '<pre id="hiddenpre" style="display: none">';
  echo htmlspecialchars(var_export($data, true));
  echo '</pre>';
}

/**
 * Prints response stream.
 * @param string $responseStream Data to print.
 */
function printJSONString($responseStream) {
  echo '<h3 style="cursor: pointer" class="handler-element-toggle-visibility" data-element-identifier="hiddencode">JSON string (click to toggle):</h3>'."\n";
  echo '<pre id="hiddencode" style="display: none">';
  echo htmlspecialchars(json_encode(json_decode($responseStream, true), JSON_PRETTY_PRINT));
  echo '</pre>';
}

/**
 * Prints form detail.
 * @param array $details Form details recived from response.
 */
function printFormDetail($details) {
  echo '<h1>'.htmlspecialchars($details['form']['name']);
  if (!empty($details['form']['is_frequent'])) {
    echo ' (Frequent form)';
  }
  if (!empty($details['form']['is_public'])) {
    echo ' (Public form)';
  }
  echo ' <a href="?"> &laquo; back to list of Forms</a></h1>';
  echo '<p>';
  if (!empty($details['form']['reports_count'])) {
    echo 'Total Responses: '.$details['form']['reports_count'].'<br />';
  }
  if (!empty($details['form']['approved_count'])) {
    echo 'Approved Responses: '.$details['form']['approved_count'].'<br />';
  }
  if (!empty($details['form']['awaiting_approval_count'])) {
    echo 'Not-approved Responses: '.$details['form']['awaiting_approval_count'].'<br />';
  }
  if (!empty($details['form_items'])) {
    echo 'Total Form Items: '.count($details['form_items']).'<br />';
  }
  echo '</p>';
}

/**
 * Prints response details.
 * @param array $details Response details recived from response.
 */
function printResponses($details) {
  echo '<h3>Received responses:</h3>';
  if (!empty($details['data'])) {
    $paging = '';
    if (!empty($_REQUEST['pg']) && $_REQUEST['pg'] > 0) {
      $paging .= '<a href="?fid='.((int)$_REQUEST['fid']).'&pg='.(((int)$_REQUEST['pg']) - 1).'">&lt; Previous Page</a>';
    }
    if ((empty($_REQUEST['pg']) && $details['form']['reports_count'] > 1000) || (floor($details['form']['reports_count'] / 1000) > (isset($_REQUEST['pg']) ? intval($_REQUEST['pg']) : 0))) {
      $paging .= '<a href="?fid='.((int)$_REQUEST['fid']).'&pg='.(isset($_REQUEST['pg']) ? intval($_REQUEST['pg']) + 1 : 1).'">Next Page &gt;</a>';
    }
    echo $paging;
    echo '<ul>'."\n";
    foreach ($details['data'] as $result) {
      //$result is one response
      echo '  <li><div style="cursor: pointer" title="Click to show values" class="handler-element-toggle-visibility" data-element-identifier="nextSibling">'.(!empty($result['event_name']) ? htmlspecialchars($result['event_name']) : '-- empty --').(!empty($result['member_label']) ? ' ('.htmlspecialchars($result['member_label']).')' : '').'</div>';
      echo '<ul style="display: none;">'."\n";
      // now let's loop over the form_items rather then the $result['data'], so we can have the items in correct order
      $lastPage = null;
      foreach ($details['form_items'] as $k=>$fdet) {
        $pg = $fdet['page_num'];
        $has_pg = isset($result['subpages'][$pg]);
        $has_item = false;
        if ($lastPage != $fdet['page_name']) {
          //to keep this file simple, we output this all the time (not knowing if there are any values on the page)
          if (!is_null($lastPage)) {
            echo "      </ul>\n    </li>\n";
          }
          echo '    <li>'.$fdet['page_name'].'<ul>'."\n";
          $lastPage = $fdet['page_name'];
        }
        ob_start();
        // beware the repeatable forms!
        for ($i = 0; $i<($has_pg?$result['subpages'][$pg]:1);$i++) {
          $_k = $k.($details['can_contain_repeatable_section'] ? '_'.$i : '');
          if (!isset($result[$_k])) {
            continue;
          }
          if ($has_pg) {
            echo '<br />'."\n        ";
          }
          $has_item = true;
          if ($has_pg) {
            echo 'cp: '.($i+1).': ';
          }
          $vals = array();
          if (isset($result[$_k]['value']) && !isset($result[$_k]['files']) && !isset($result[$_k]['comments'])) {
            // handle simple values
            $vals[] = htmlspecialchars($result[$_k]['value']);
          }
          if (isset($result[$_k]['files'])) {
            // handle images + other files like MP3's
            $files = [];
            foreach ($result[$_k]['files'] as $f) {
              $files[] = '<a target="_blank" href="'.$f['file_url'].'">'.(!empty($f['thumbnail_url']) ? '<img src="'.htmlspecialchars($f['thumbnail_url']).'" alt="image thumbnail" '.(strpos($f['thumbnail_url'], 'site/images') !== false ? 'style="width: 36px"' : '').'/>' : basename($f['file_url'])).'</a>';
            }
            $vals[] = implode(' ', $files);
          }
          if (isset($result[$_k]['comments'])) {
            // handle comments
            $vals[] = implode(", ", array_map('htmlspecialchars', $result[$_k]['comments']));
          }
          echo implode('<br />', $vals);
        }
        $cnt = ob_get_clean();
        //let find out if we have any data!
        if ($has_item || !empty($show_empty)) {
          echo '      <li data-field_id="'.intval($k).'">'.htmlspecialchars($fdet['name']).': ';
          echo $cnt."\n";
          echo '      </li>'."\n";
        }
      }
      if (!is_null($lastPage)) {
        echo "      </ul>\n    </li>\n";
      }
      echo '  </ul>';
      echo '</li>'."\n";
    }
    echo '</ul>'."\n";
    echo $paging;
  } else {
    echo '<p>The form '.htmlspecialchars($details['form']['name']).' appears to have no responses</p>'."\n";
  }
}
