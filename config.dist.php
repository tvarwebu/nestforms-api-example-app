<?php
/**
 * To get OAUTH keys you have to go to http://www.nestforms.com/Applications and register the application.
 * You should use the name of the directory the index.php is in for the Redirect URIs.
 * If you have a domain http://www.example.com and the index.php is in some/directory/index.php the Redirect URI is 'http://www.example.com/some/directory/' and 'http://www.example.com/some/directory/index.php'
 * After the app is saved go to tab 'Keys' where you will find the Client ID and Client Secret keys.
 */
define('OAUTH_CLIENT_ID', 'REPLACE_ME');
define('OAUTH_CLIENT_SECRET', 'REPLACE_ME');
define('OAUTH_REDIRECT_URL', ''); // << put in the redirect url, if you don't want to use the same script or it doesn't work correctly for you. eg: http://localhost/nestforms_apitests/index.php

$host = 'https://www.nestforms.com';
$googleTagManagerId = ''; // nestforms: GTM-WN9K3TL
