<?php

require_once(dirname(__FILE__).'/config.php');
require_once(dirname(__FILE__).'/functions_authorization.php');
require_once(dirname(__FILE__).'/functions_example_forms_responses_customdb.php');
require_once(dirname(__FILE__).'/functions_example_remote_login.php');

session_start();
ob_start();

pageHeaders();

printHead();

printPageHeader();

if (!is_dir(dirname(__FILE__).'/vendor')) {
  exit('<div style="color: red;">Please install the vendor using <pre>composer install</pre> command. If you don\'t have composer download required files from <a href="http://www.nestforms.com/files/nestforms-api-vendor.zip" target="_blank">http://www.nestforms.com/files/nestforms-api-vendor.zip</a> and unzip it. You should get a "vendor" directory next to the index.php file.</div>');
}

require_once(dirname(__FILE__).'/vendor/autoload.php');

if (!defined('OAUTH_CLIENT_ID') || !defined('OAUTH_CLIENT_SECRET') || OAUTH_CLIENT_ID === 'REPLACE_ME' || OAUTH_CLIENT_SECRET === 'REPLACE_ME') {
  printInstructions();
  echo '<div style="color: red;">You do not have set the config.php variables yet (step 3).</div>';
  exit();
}

if (!empty($_REQUEST['token_revoke']) && in_array($_REQUEST['token_revoke'], ['example_forms_responses_customdb', 'example_remote_login'])) {
  if (revokeAccessToken($_REQUEST['token_revoke'])) {
    echo 'Revoke token for '.$_REQUEST['token_revoke'].'.';
  } else {
    echo 'Could not revoke token for '.$_REQUEST['token_revoke'].'.';
  }
}

$currentPage = getCurrentPage();

if (!empty($currentPage)) {
  if (!in_array($currentPage, ['example_forms_responses_customdb', 'example_remote_login'])) {
    $currentPageDisplay = preg_replace('/(h\s*t\s*t\s*p\s*s?\s*:\s*\/\s*\/)|((w\s*){3}\.)/i', '', $currentPage);
    if (strlen($currentPageDisplay) > 33) {
      $currentPageDisplay = substr($currentPageDisplay, 0, 30) . '...';
    }
    exit('<div style="color: red;">Unknown page: <i>'.$currentPageDisplay.'</i></div>');
  }
  $_SESSION['current_page'] = $currentPage;
  $provider = getProvider(getScopesForExample($currentPage));
  $accessToken = getAccessToken($provider, $currentPage);

  printMenu();

  if ($accessToken === false) {
    // we are missing the access token / the access token was not able to refresh, redirect the user to the authorization again
    header('HTTP/1.1 302 Found');
    header('Location: '.$api->getAuthorizeUri($context));
    exit;
  }

  if (isset($_REQUEST['hide_tokens'])) {
    $_SESSION['hide_tokens'] = $_REQUEST['hide_tokens'];
  }
  if (empty($_SESSION['hide_tokens'])) {
    printToken($accessToken);
  }

  $client = new GuzzleHttp\Client(array('base_uri' => $host.'/api/'));

  if ($currentPage === 'example_forms_responses_customdb') {
    exampleFormsResponsesCustomdb($accessToken, $provider, $client);
  } elseif ($currentPage === 'example_remote_login') {
    exampleRemoteLogin($accessToken, $provider, $client);
  }
} else {
  printInfo();
}

printFooter();

function pageHeaders() {
  Header("Content-type: text/html; charset=UTF-8");
}

function printHead() {
  echo '<!DOCTYPE html>
<html lang="en">
  <head>
    <title>API Example App for NestForms.com service</title>
    <meta name="description" content="Online testing environment for NestForms API. Test if the NestForms API can export data you require into your internal system." />
    <meta name="robots" content="index, nofollow" />
    <script src="event_handlers.js"></script>
  </head>
  <body>
';
}
function printFooter() {
  global $googleTagManagerId, $host;
  /**
   * If you are using Content-Security-Policy make sure to generate the SHA for following javascript code.
   */
  echo '<script>'."\n";
  echo '  var host = "'.$host.'";'."\n";
  if (!empty($googleTagManagerId)) {
    echo '  window.dataLayer = window.dataLayer || [];'."\n";
    echo '  (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\':new Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src=\'https://www.googletagmanager.com/gtm.js?id=\'+i+dl;f.parentNode.insertBefore(j,f);})(window,document,\'script\',\'dataLayer\',\''.$googleTagManagerId.'\');'."\n";
  }
  echo '</script>'."\n";
  echo '</body>
</html>';
}
function printMenu() {
  $accessTokens = [];
  foreach (['example_forms_responses_customdb', 'example_remote_login'] as $page) {
    $provider = getProvider(getScopesForExample($page));
    $accessTokens[$page] = getAccessToken($provider, $page, true);
  }

  echo '<a href="?page=home">Home</a><br />';
  echo '<a href="?page=example_forms_responses_customdb">Working with Forms, Responses and Custom DB</a>';
  if (!empty($accessTokens['example_forms_responses_customdb'])) {
    echo ' <a href="?token_revoke=example_forms_responses_customdb">revoke token</a>';
  }
  echo '<br />';
  echo '<a href="?page=example_remote_login">Remote login</a>';
  if (!empty($accessTokens['example_remote_login'])) {
    echo ' <a href="?token_revoke=example_remote_login">revoke token</a>';
  }
  echo '<br />';
}
function printToken($accessToken) {
  echo '<p>
    Access Token: <span style="cursor: pointer;" class="handler-element-replace-text-content" data-text-content-new="\''.$accessToken->getToken().'\'">'.substr($accessToken->getToken(), 0, 4).'*****'.substr($accessToken->getToken(), -4).'</span> (expires in '.accessTokenLeft($accessToken).') 
    Refresh Token: <span style="cursor: pointer;" class="handler-element-replace-text-content" data-text-content-new="\''.$accessToken->getRefreshToken().'\'">'.substr($accessToken->getRefreshToken(), 0, 4).'*****'.substr($accessToken->getRefreshToken(), -4).'</span>
  </p>';
}
function printPageHeader() {
  global $host;
  echo '
    <a style="float: right;" href="'.$host.'"><img src="'.$host.'/site/images/logo.png" alt="nestforms logo"></a>
    <h1>Nestforms API examples</h1>
  ';
}
function printInstructions() {
  echo '
    <div>
      For both examples you need to make several steps to get it running:
      <ol>
        <li>Create an application in the nestforms.com/Applications website.</li>
        <li>Add your current test application URL into the application you have created in step 1.</li>
        <li>Update the config.php file within this example API to add the <strong>Client ID</strong> and the <strong>Client Secret</strong> that you\'ve received in step 1.</li>
        <li>Open the index.php in your browser.</li>
      </ol>
    </div>
  ';
}
function printInfo() {
  printInstructions();
  echo '
    <div>
      Click into one of the links below, you will be redirected to the nestforms.com website (you might be asked to login first) where you will confirm the access level rights and then you will be redirected back to this example application. 
      NestForms is using a standard oAuth authentication process where you will receive a refresh token (valid for 30 days) and access token (valid for 1 hour).
      Examples below can automatically request an updated Access token when the current one is about to expire.
    </div>

    <h2>The example application covers two use cases</h2>
    <a href="?page=example_forms_responses_customdb"><strong>Working with Forms, Responses and Custom DB</strong></a>
    <div>
      This example will show you following features:
      <ul>
        <li>A list of all available <strong>forms</strong>. Once you click to the form, you will see all <strong>Responses</strong>.</li>
        <li>You can list all <strong>related members</strong>.</li>
        <li>You can also list the <strong>custom DB tables</strong> within your NestForms account including the option to update the values.</li>
      </ul>
    </div>
    <a href="?page=example_remote_login" style="margin-top: 20px; display: block;"><strong>Remote Login</strong></a>
    <div>
      This example will let you generate a special URL using the API.
      Then using this URL will allow you to display the NestForms website inside the iframe. This might be useful when for example you want to login your client automatically within your intranet without the need for them to login.
  </div>
  ';
}

/**
 * Returns page from request or session.
 * @return string $currentPage
 */
function getCurrentPage() {
  $currentPage = isset($_REQUEST['page']) && is_string($_REQUEST['page']) ? htmlspecialchars($_REQUEST['page'], ENT_QUOTES) : '';
  if (empty($currentPage) && !empty($_SESSION['current_page'])) {
    $currentPage = $_SESSION['current_page'];
  }
  if ((!empty($currentPage) && $currentPage === 'home') || (isset($_REQUEST['error']) && $_REQUEST['error'] === 'access_denied')) {
    unset($_SESSION['current_page']);
    return '';
  }

  return $currentPage;
}

/**
 * Returns correct scopes for requred example.
 * @param string $exampleName Name of example must be one of ['example_forms_responses_customdb', 'example_remote_login'].
 * @return array<string> Array containing scopes for example.
 */
function getScopesForExample($exampleName) {
  if ($exampleName === 'example_forms_responses_customdb') {
    return [
      'forms_access', // needed to read informations about forms
      'reports_read', // needed to read reports
      'customdb_read', // needed to read custom db data
      'customdb_write' // needed to write to custom db
    ];
  } elseif ($exampleName === 'example_remote_login') {
    return [
      'remote_login', // needed to create remote login url
    ];
  }
  return [];
}
