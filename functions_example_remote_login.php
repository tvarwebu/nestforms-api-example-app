<?php
function exampleRemoteLogin($accessToken, $provider, $client) {
  global $host;

  try {
    $response = $client->send($provider->getAuthenticatedRequest(
      'POST',
      $host.'/api/v2/members/me/remote_login_create', // Returns one time login url.
      $accessToken
    ));
    $responseBody = json_decode($response->getBody(), true);
    $link = $responseBody['result']['link'].'?'.http_build_query([
      'location' => $host.'/Form-List', // Controls where user will be redirected to after login, default is dashboard.
      'allow_iframes' => true, // Allow the loading of NestForms inside iframe, default is false.
      'remove_page_header' => true // Remove the NestForms header, default is false.
    ]);
    echo '<div>See details about window messaging at the bottom of the page.</div>';
    echo '<iframe id="iframeNestforms" width="1280" height="720" src="'.$link.'" style="display: block; margin-left: auto; margin-right: auto;"></iframe><br>';
    printRemoteLoginInformation();
  } catch (GuzzleHttp\Exception\ClientException $ex) {
    // if an error occurs
    echo '<div style="color: red;">Failed to fetch the data, you don\'t seem to have the required permissions</div>';
  }
}

function printRemoteLoginInformation() {
  global $host;

  echo '
    <div>
      You can use cross window messaging to react to certain events. Following events are sent:
      <ul>
        <li><strong>redirect</strong> - sent when the page is redirecting to another page, url that page is redirecting to is send as additional parameter called url.</li>
        <li><strong>pageSave</strong> - sent when page is saved.</li>
        <li><strong>loginRequired</strong> - sent when page requires user to login because session expired.</li>
      </ul>
      This is only partially applied, please test on the Form Builder. If you would like us to expand, Contact us and let us know what pages you require these messages for.
      Example message: 
      <pre>
        window.parent.postMessage({ event: \'redirect\', url: \''.$host.'/Form-List\' }, \'*\');
      </pre>
      Ideal way to check this in your parent window: 
      <pre>
        window.addEventListener("message", (message) => {
          if (message.origin === "'.$host.'") {
            //TODO: your process code to be placed here
            console.log("Received message: " + message.data.event)
          }
        }, false);
      </pre>
    </div>
  ';
}
