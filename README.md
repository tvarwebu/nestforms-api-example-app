# Nestforms API oAuth2 client
License: GPLv2

# Description

This applications allows you to access the www.nestforms.com API using a standardized oAuth v2.0 protocol.

# Features

* Authenticate with www.nestforms.com
* Refresh Auth tokens, when needed
* List all available forms for the member
* Show all responses for a form
* List all Custom DB Tables, their groups with ability to add new, edit and delete existing
* List all related members
* Create link for automatically logging in, which can be used in iframes

Further details can be found on https://www.nestforms.com/Help-Applications

# Installation

* Download the ZIP file
* unzip it somewhere on your website (The WWW server must be able to process PHP scripts)
* Install the libraries
  * If you have composer, run `composer update` within the directory,
  * If you don't then download required files from http://www.nestforms.com/files/nestforms-api-vendor.zip
    and unzip it. You should get a 'vendor' directory next to the index.php file.
* Go to http://www.nestforms.com/Applications and register the application.
  
  You should use the name of the directory the index.php is in for the Redirect URIs so for example, if you have a domain http://www.example.com and the index.php lay in some/directory/index.php
  the Redirect URI is 'http://www.example.com/some/directory/' and 'http://www.example.com/some/directory/index.php'
* After the app is saved go to tab 'Keys' and gather the Client ID and Client Secret keys.
* Copy the config.dist.php to config.php
* Fill these keys into the new config.php OAUTH_CLIENT_ID and OAUTH_CLIENT_SECRET respectively.
* Now open the script by accessing the Redirect URI You've filled in the http://www.nestforms.com/Applications page,
    
    You should be redirected to the www.nestforms.com website to approve the request.
    After this approval is made, you will be redirected back to your script.
