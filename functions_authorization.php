<?php

/**
 * URL to redirect to after autorization.
 * @return string redirect uri
 */
function oauthRedirectUrl() {
  if (empty(OAUTH_REDIRECT_URL) && isset($_SERVER['REQUEST_SCHEME']) && isset($_SERVER['HTTP_HOST']) && isset($_SERVER['REQUEST_URI'])) {
    $parsed = parse_url($_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
    return $parsed['scheme'].'://'.$parsed['host'].(!empty($parsed['port']) ? ':'.$parsed['port'] : '').''.$parsed['path'];
  }

  return OAUTH_REDIRECT_URL;
}

/**
 * Returns provider for request with defined scopes.
 * @param array<string> $scopes array of scopes. Available scopes: [ 'forms_access', 'reports_read', 'customdb_read', 'customdb_write', 'remote_login' ].
 * @return \League\OAuth2\Client\Provider\GenericProvider $provider
 */
function getProvider($scopes) {
  global $host;
  if (empty($scopes)) {
    exit('<div style="color: red;">Cannot create provider without scopes.</div>');
  }

  return new \League\OAuth2\Client\Provider\GenericProvider([
    'clientId'                => OAUTH_CLIENT_ID,    // The client ID assigned to you by the provider.
    'clientSecret'            => OAUTH_CLIENT_SECRET,   // The client password assigned to you by the provider.
    'redirectUri'             => oauthRedirectUrl(),
    'urlAuthorize'            => $host.'/api/authorize',
    'urlAccessToken'          => $host.'/api/access_token',
    'urlResourceOwnerDetails' => $host.'/api/member',
    'scopeSeparator' => ' ',
    'scopes' => $scopes,
  ]);
}

/**
 * Returns access token for specified provider.
 * @param \League\OAuth2\Client\Provider\GenericProvider $provider Provider retuned from function getProvider().
 * @param string $currentPage Used to separete access tokens for different examples in session.
 * @param bool $returnOnly Return the token if created without trying to create it.
 * @return League\OAuth2\Client\Token\AccessToken $accessToken
 */
function getAccessToken($provider, $currentPage, $returnOnly = false) {
  $accessToken = null;

  // If we don't have an authorization code then get one.
  if (!empty($_SESSION['access_token_'.$currentPage])) {
    $accessToken = new League\OAuth2\Client\Token\AccessToken($_SESSION['access_token_'.$currentPage]);
    if ($accessToken->hasExpired()) {
      //FIXME: We need to implement refresng of the token?.
      $accessToken = $provider->getAccessToken('refresh_token', [
        'refresh_token' => $accessToken->getRefreshToken()
      ]);
      $_SESSION['access_token_'.$currentPage] = $accessToken->jsonSerialize();
    }
  }

  if ($returnOnly) {
    return $accessToken;
  }

  if (empty($accessToken)) {
    if (!isset($_GET['code'])) {
      // Fetch the authorization URL from the provider; this returns the.
      // urlAuthorize option and generates and applies any necessary parameters.
      // (e.g. state).
      $authorizationUrl = $provider->getAuthorizationUrl();

      // Get the state generated for you and store it to the session.
      $_SESSION['oauth2state'] = $provider->getState();

      // Redirect the user to the authorization URL.
      header('Location: ' . $authorizationUrl);
      exit;

      // Check given state against previously stored one to mitigate CSRF attack.
    } elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {
      unset($_SESSION['oauth2state']);
      exit('Invalid state');
    }
    $accessToken = $provider->getAccessToken('authorization_code', [
        'code' => $_GET['code']
    ]);
    $_SESSION['access_token_'.$currentPage] = $accessToken->jsonSerialize();
  }

  return $accessToken;
}

/**
 * Calc how long the access token lasts
 * @param League\OAuth2\Client\Token\AccessToken $accessToken Access token for witch to calculate remaining time.
 * @return string $left Time left.
 */
function accessTokenLeft($accessToken) {
  $left = ($accessToken->getExpires()) - time();
  if ($left > 3600) {
    $h = floor($left / 3600);
    $left = $h.'h '.(($left/60) - round($h * 60)).'m';
  } elseif ($left > 60) {
    $m = floor($left / 60);
    $left = $m.'m '.($left - ($m * 60)).'s';
  } else {
    $left = $left .'s';
  }
  return $left;
}

/**
 * @param string $page Page to remove the token for.
 */
function revokeAccessToken($page) {
  global $host;

  $provider = getProvider(getScopesForExample($page));
  $accessToken = getAccessToken($provider, $page, true);

  if (empty($accessToken)) {
    return true;
  }

  $client = new \GuzzleHttp\Client(array('base_uri' => $host.'/api/'));
  $response = $client->send($provider->getAuthenticatedRequest(
    'DELETE',
    $host.'/api/v2/access_token',
    $accessToken
  ));
  $responseBody = json_decode($response->getBody(), true);
  if (!empty($responseBody['result'])) {
    unset($_SESSION['access_token_'.$page]);
    unset($_SESSION['current_page']);
    return true;
  }
  return false;
}
